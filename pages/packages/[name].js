import React, { useState, useEffect } from 'react'
import Entry from '../../components/Entry'
import dpkgService from '../../services/dpkgService'

const Package = ({ initialPackage }) => {
  const [pkg, setPkg] = useState(initialPackage)

  useEffect(() => {
    setPkg(initialPackage)
  }, [initialPackage])

  if (!pkg) {
    return <div>Loading...</div>
  } else {
    return (
      <Entry pkg={pkg} />
    )
  }
}

Package.getInitialProps = (context) => {
  const pkg = dpkgService.get(context.query.name)

  if (!pkg) {
    return { initialPackage: { name: context.query.name, description: 'Attention: This package was not found in the current dpkg file.' } }
  }

  const reverseDependencies = dpkgService.getReverseDependencies(pkg.name)
  return { initialPackage: { ...pkg, reverseDependencies: reverseDependencies } }
}

export default Package
