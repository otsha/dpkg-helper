import React from 'react'
import Link from 'next/link'

const Entry = ({ pkg }) => {
  return (
    <div>
      <h2>{pkg.name}</h2>
      <p>{pkg.description}</p>
      <h4>Depends on:</h4>
      {pkg.depends && pkg.depends.length > 0
        ? <ul>
          {pkg.depends.map((dependency) =>
            <li>
              <Link
                href='/packages/[name]'
                as={`/packages/${dependency}`} >
                <a>{dependency}</a>
              </Link>
            </li>
          )}
        </ul>
        : <p>This package does not depend on any other packages.</p>
      }

      <h4>{`Packages depending on ${pkg.name}:`}</h4>
      {pkg.reverseDependencies && pkg.reverseDependencies.length > 0
        ? <ul>
          {pkg.reverseDependencies.map((dependency) =>
            <li>
              <Link
                href='/packages/[name]'
                as={`/packages/${dependency}`} >
                <a>{dependency}</a>
              </Link>
            </li>
          )}
        </ul>
        : <p>{`No other package depends on ${pkg.name}`}</p>
      }
    </div>
  )
}

export default Entry
