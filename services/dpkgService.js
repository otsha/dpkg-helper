/**
 * Offers parsing for the dpkg status file and
 * keeps track of the current list of packages.
 */

let currentPackages = []

/* Find and return a single package with the given name. */
const get = (name) => {
  return currentPackages.find((pkg) => pkg.name === name)
}

/* Re-set the list of packages */
const set = (newPackages) => {
  currentPackages = newPackages
  return currentPackages
}

/* Return the current list of packages. */
const getAll = () => {
  return currentPackages
}

/* Find all the packages that list the given package as a dependency */
const getReverseDependencies = (name) => {
  return currentPackages.filter((p) => p.depends.includes(name)).map((p) => p.name)
}

/* Parse packages from the given text */
const parse = (text) => {

  /* Define the attributes we wish to extract */
  const attributes = ['Package', 'Description', 'Depends']

  /* According to the syntax, each package is separated with an empty line */
  let packages = text.split('\n\n')

  /* Extract only the rows with the attributes we're interested in */
  packages = packages.map((pack) =>
    pack.split('\n')
      .map((a) => a.split(': '))
      .filter((a) => attributes.includes(a[0]))
  )

  /* Parse the packages so that the attributes are in a readily usable form */
  packages = packages.map((pack) => {
    const packageName = pack.find((attribute) => attribute[0].startsWith('Package'))[1]
    const packageDescription = pack.find((attribute) => attribute[0].startsWith('Description'))[1]
    const hasDepends = pack.find((attribute) => attribute[0].startsWith('Depends'))
    const packageDependsOn = new Set()

    hasDepends && hasDepends[1].split(', ').forEach((dependency) => {
      packageDependsOn.add(dependency.split(' ')[0])
    })

    const depends = [...packageDependsOn]

    return {
      name: packageName,
      description: packageDescription,
      depends: depends
    }
  })

  /**
   * Note: reverse dependencies were not parsed at this stage,
   * as they are only displayed when an user views the information of a package.
   * It is enough to fetch them for each package separately.
   * Besides, crawling through all the reverse dependencies at once will definitely not be efficient. 
   */

  /* Finally, sort the package list to be in alphabetical order (A-Z) */
  const sorted = packages.sort((a, b) => a.name > b.name ? 1 : -1)

  return sorted
}

export default { get, set, getAll, getReverseDependencies, parse }
