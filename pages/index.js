import React, { useState, useEffect } from 'react'
import FileInput from '../components/FileInput'
import Link from 'next/link'
import dpkgService from '../services/dpkgService'

const Main = () => {
  const [packages, setPackages] = useState([])

  useEffect(() => {
    setPackages(dpkgService.getAll())
  }, [])

  const loadFromFile = (event) => {
    const reader = new FileReader()
    const file = event.target.files[0]

    reader.onload = () => {
      const readText = reader.result
      const parsedPackages = dpkgService.parse(readText)
      setPackages(dpkgService.set(parsedPackages))
    }

    reader.onerror = () => {
      window.alert('Error reading file:', reader.error)
    }

    reader.readAsText(file, 'utf-8')
  }

  return (
    <div>
      <h1>DPKG Helper</h1>
      <FileInput handleLoad={loadFromFile} />
      <ul>
        {packages.map((pkg) =>
          <li key={pkg.name}>
            <Link
              href='/packages/[name]'
              as={`/packages/${pkg.name}`} >
              <a>{pkg.name}</a>
            </Link>
          </li>
        )}
      </ul>
    </div>
  )
}

export default Main
