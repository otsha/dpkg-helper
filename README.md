dpkg-helper
===========
A tiny web application to help browsing through the `var/lib/dpkg/status` file on Debian systems. Built with [React](https://reactjs.org/) and [Next.js](https://reactjs.org/).

- [Application on Heroku]() - Please note that the page might take a while to load initially due to the limits of a free Heroku account.
- To run locally:
    - Clone this repository
    - Run `npm install && npm run dev` to install dependencies and start the application in development mode
    - Navigate to `localhost:3000` in your web browser of choice

Why React?
----------
Simply put, React is a tool I have used before and am comfortable with.

Why Next.js?
-----------
As the application does not need to serve APIs or handle anything server-side, there was no need to configure a Node.js / Express backend manually. With the scope of this project being to create something quickly and with easy maintenance, I found Next.js to be a fitting tool as it is lightweight and straightforward to set up and deploy.

Strengths
---------
- Simplicity: Does just what it needs to
- Easily deployable thanks to the Next.js framework

Weaknesses
----------
- No unit testing: the application has only been tested manually
    - Decision was partially affected by the requirement to keep dependencies minimal
- Only the shord description for each package is shown

Potential Further Improvements
------------------------------
- Show further info about the packages, such as the full description, version numbers and websites
- Add a home button that returns to the main view
- Add a search/filter bar to the main view to help users find the packages they are looking for
- Further improve the visual outlook of the application with CSS
