import React from 'react'
import dpkgService from '../services/dpkgService'

const FileInput = ({ handleLoad }) => {
  return (
    <div>
      <hr/>
      <p>Select your <code>var/lib/dpkg/status</code> file:</p>
      <input type='file' onChange={handleLoad} />
      <hr/>
    </div>
  )
}

export default FileInput